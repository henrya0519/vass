import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL } from '../config/variables';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  api: string = URL;

  constructor(
    private http: HttpClient,
  ) { }

  getusers() {
    return this.http.get <any>(`${this.api}/get_users`);
  }

  getuser(payload) {
    return this.http.post <any>(`${this.api}/find_user`, payload);
  }

  createusers(payload) {
    return this.http.post <any>(`${this.api}/create_user`, payload);
  }
}
