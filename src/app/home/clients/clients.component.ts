import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormsModule } from '@angular/forms';
import { data } from 'jquery';
import { UsersService} from '../../services/users.service'

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {
  loginForm = new FormGroup({
    businessid: new FormControl(null, Validators.required),
    phone: new FormControl(null, Validators.required),
    email: new FormControl(null, Validators.required),
    date: new FormControl(null, Validators.required),
    end: new FormControl(null, Validators.required),
  });

  search = new FormGroup({
    sharedkey: new FormControl(null, Validators.required)
  });
  

  items= []
  constructor(
    private user: UsersService
    ) { }

  ngOnInit(): void {
   
    this.getUsers();
  }

  getValues(sharedKey,businessID,email,phone){
    console.log('datos: ', sharedKey, businessID, email, phone);
  }

  readValues(){
    const dataIn = this.loginForm.value;
    this.user.createusers(dataIn)
    .subscribe(resp =>{
      this.getUsers();
    },
    err =>{
    })
  }
  sear(){
    const dataIn = this.search.value;
    if(dataIn.sharedkey){
      this.user.getuser(dataIn)
      .subscribe(resp =>{
        this.items = resp;
      },
      err =>{
      })
    }
    else{
      this.getUsers()
    }
    
  }

  getUsers()
  {
    this.user.getusers()
    .subscribe(resp =>{
      this.items = resp;
    },
    err =>{
    })

  }


 

}
